# dckr node Sanje eev

init npm  
images/2023-05-13-08-34-35.png

install express  
![](images/2023-05-13-08-35-33.png)

index.js root  
![](images/2023-05-13-08-36-07.png)

node index.js - run app  
![](images/2023-05-13-08-36-43.png)

test in browser 3000  
![](images/2023-05-13-08-37-09.png)

Dockerfile  - npm install   
![](images/2023-05-13-08-40-03.png)

![](images/2023-05-13-08-44-34.png)

docker build .  
![](images/2023-05-13-08-45-03.png)

docker image ls  
![](images/2023-05-13-08-46-22.png)

delete image and build again with name  
![](images/2023-05-13-08-46-58.png)

docker run and name container  
![](images/2023-05-13-08-48-10.png)

docker ps -- izlista sve kontejnere otvorene  
![](images/2023-05-13-08-48-33.png)

obrisi kontejner -f znaci da ga mozes obrisati dok jos radi  
![](images/2023-05-13-08-54-01.png)

run container  
![](images/2023-05-13-09-04-51.png)

0000 znaci da bilo koji host moze slat req  
![](images/2023-05-13-09-05-35.png)

udi unutra u kontejner  
![](images/2023-05-13-09-06-23.png)

kad izlistamo imamo unutra kopiju svega - to nije dobro (moze se kopirati i environment var i node_modules)
![](images/2023-05-13-09-07-00.png)

docker rm ovaj kont sa svime  
![](images/2023-05-13-09-09-10.png)

.dockerignore  
![](images/2023-05-13-09-09-30.png)

build container  
![](images/2023-05-13-09-10-02.png)

run  
![](images/2023-05-13-09-10-37.png)

provjera jel radi dockerignore (radi, ovi node_modules je novo zbog npm install, nije kopiran izvan kontejnera)
![](images/2023-05-13-09-11-13.png)

obrisi kont. ovo nije dobar nacin, treba imat volumes zato da ne trebas buildat cont svaki put  
![](images/2023-05-13-09-20-25.png)

volumes, mozes staviti cijeli path (ali ne treba ovak, ovo je primjer)  
![](images/2023-05-13-09-23-23.png)

razlicite komande za path, prvo za cmd prompt win  
![](images/2023-05-13-09-24-33.png)

powershell working dir  
![](images/2023-05-13-09-25-03.png)

linux/mac je obicne zagrade a ne viticaste  
![](images/2023-05-13-09-25-38.png)

install nodemon as dev dependency for refresh  
![](images/2023-05-13-09-41-48.png)

dodaj start i dev u package.json  
![](images/2023-05-13-09-42-42.png)

nodemon -L flag za neke situacije koje nisu ceste da se refresha uvijek (kad se koristi docker na win)
![](images/2023-05-13-09-44-11.png)

izbrisi cont (opet)  
![](images/2023-05-13-09-44-54.png)

build (ovaj put je duze zato sto nema cache jer je novi paket u package.json)  
![](images/2023-05-13-09-46-18.png)

npm run dev u dockerfile stavi umjesto node index.js  
![](images/2023-05-13-09-47-28.png)

rebuild image (opet)  
![](images/2023-05-13-09-48-02.png)

run image (opet i to s volume)  
![](images/2023-05-13-09-48-35.png)

moze se obrisati node_modules izvan kontejnera (ionako se ne koristi)  
![](images/2023-05-13-09-49-47.png)

razlika docker ps i docker ps -a je u tome sto je docker ps za sve kontejnere koji rade, a ps -a za sve koji rade i ne rade  
![](images/2023-05-13-09-51-08.png)

app se srusila jer smo obrisali node_modules, a to provjerimo s logovima, a srusilo se jer imamo BIND volumes (koji se pozivaju s -v) koji sinkroniziraju oba direktorija i prebrisu ono kaj je u cont  
![](images/2023-05-13-09-52-03.png)

da se ne bi prebrisalo node_modules unutar kont, ako se izbrise izvan, mozemo to sprijeciti tako da upotrijebimo poseban -v za node modules, koji ima visi specificity, pa se onda nece prebrisati jer ide nakon $(pwd):/app  
![](images/2023-05-13-09-56-45.png)

bind mount zapisuje van kont sve ono sto je unutar kont, ako se stavi samo -v $(pwd):/app, ali ako se stavi na kraj :ro (read only), onda je read only i ne zapisuje van kontejnera ono sto se instalira unutra  
![](images/2023-05-13-11-11-29.png)

za provjeriti, mozemo uci u kont i pokusati napraviti file, ali nece nam dopustiti  
![](images/2023-05-13-11-12-44.png)

env var, u Dockerfile napisati ENV PORT i expose  
![](images/2023-05-13-11-16-24.png)

nakon promjene Dockerfilea, moras opet obrisat i buildat  
![](images/2023-05-13-11-17-36.png)

env varijabla kad se runa, moze biti --env ili -e flag (treba promijeniti port za kont, jer var mijenja PORT unutar nodea)  
![](images/2023-05-13-11-19-40.png)

process.env.PORT promjeniti u index.js  
![](images/2023-05-13-11-20-53.png)

ako imas vise env var mozes jednu iza druge u command --env PORT=4000 --env NESTO="nesto", ali mozes i .env file importat u run command  
![](images/2023-05-13-11-26-30.png)

env file (naravno jos je duza komanda, samo je skraceno ovdje, fale portovi, ime, detach..) 
![](images/2023-05-13-11-27-41.png)

udi u kont i provjeri sve envove sa printenv kom   
![](images/2023-05-13-11-51-11.png)

volumes ostaju zapisani nakon sto rm kont  
![](images/2023-05-13-11-52-25.png)

docker volume prune cisti sve nepotrebne volumse  
![](images/2023-05-13-11-53-13.png)

da bi se automatski obrisao volume onda koristis flag -fv da odmah obrises kad ides micat Cont  
![](images/2023-05-13-11-54-36.png)

=> 1:06:00 Docker compose STOP <=  
![](images/2023-05-13-11-58-36.png)