# Ja_vaTe_chie docker springboot
![](images/2023-05-10-03-19-37.png)

config springboot  
![](images/2023-05-10-03-20-00.png)
![](images/2023-05-10-03-21-03.png)

GET /message  
![](images/2023-05-10-03-21-38.png)

Dockerfile in root  
![](images/2023-05-10-03-22-42.png)

run spring app  
![](images/2023-05-10-03-23-09.png)

build image  
![](images/2023-05-10-03-23-45.png)

docker run image  
![](images/2023-05-10-03-24-20.png)

test in browser localhost  
![](images/2023-05-10-03-25-10.png)

test in browser docker image  
![](images/2023-05-10-03-25-39.png)

