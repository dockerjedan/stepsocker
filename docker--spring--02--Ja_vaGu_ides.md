# doc ker Spr ing Ja_vaGu_ides
![](images/2023-05-10-05-05-56.png)

generate config  
![](images/02-Jam_vamGui_3351-2023-05-10-05-16-51.png)

main/java/demo.java  @Get  
![](images/2023-05-10-05-18-30-02-Jam_vamGu_i3330.png)

create jar package with command ./mvnw package  
![](images/2023-05-10-05-48-08.png)

Dockerfile  
![](images/2023-05-10-05-20-38-02-Jam_vamGu_i3338.png)

docker build  
![](images/2023-05-10-05-21-50-02-Jam_vamGu_i3350.png)

build result  
![](images/2023-05-10-05-22-11-02-Jam_vamGu_i3311.png)

check docker images  
![](images/2023-05-10-05-22-29-02-Jam_vamGu_i3329.png)

docker run  
![](images/2023-05-10-05-22-59-02-Jam_vamGu_i3359.png)

docker run result  
![](images/2023-05-10-05-23-24-02-Jam_vamGu_i3324.png)

test /welcome endpoint GET in browser  
![](images/2023-05-10-05-23-47-02-Jam_vamGu_i3347.png)
